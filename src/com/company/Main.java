package com.company;

import Game.*;
import com.sun.org.apache.bcel.internal.generic.NEW;

public class Main {

    public static void main(String[] args) {
        Lux lux = new Lux("拉克斯");
        Gragas gragas = new Gragas("古拉格斯");
        Nasus nasus = new Nasus("納瑟斯");
        Jax jax = new Jax("賈克斯");
        Flora flora = new Flora("菲歐拉");
        Fizz fizz = new Fizz("飛斯");
        doFizz(fizz);
        doFlora(flora);
        doGragas(gragas);
        doJax(jax);
        doLux(lux);
        donasus(nasus);
    }
    public static void doLux(Roles roles){
        System.out.println(roles);
        roles.doAttack();
        roles.doQ("光明束縛");
        roles.doW("稜光障壁");
        roles.doE("光明異點");
        roles.doSpecialAttack("終極閃光");
    }
    public static void doGragas(Roles roles){
        System.out.println(roles);
        roles.doAttack();
        roles.doQ("滾動酒桶");
        roles.doW("醉酒狂暴");
        roles.doE("肉彈衝擊");
        roles.doSpecialAttack("爆破酒桶");
    }
    public static void donasus(Roles roles){
        System.out.println(roles);
        roles.doAttack();
        roles.doQ("虹吸打擊");
        roles.doW("枯萎");
        roles.doE("靈魂烈焰");
        roles.doSpecialAttack("死神降臨");
    }
    public static void doJax(Roles roles){
        System.out.println(roles);
        roles.doAttack();
        roles.doQ("跳斬");
        roles.doW("蓄力一擊");
        roles.doE("反擊風暴");
        roles.doSpecialAttack("武器專精");
    }
    public static void doFlora(Roles roles){
        System.out.println(roles);
        roles.doAttack();
        roles.doQ("移形之體");
        roles.doW("斗轉之力");
        roles.doE("迅雷之技");
        roles.doSpecialAttack("頂尖對決");
    }
    public static void doFizz(Roles roles){
        System.out.println(roles);
        roles.doAttack();
        roles.doQ("現撈海膽");
        roles.doW("海神三叉戟");
        roles.doE("愛玩小飛");
        roles.doSpecialAttack("海之霸主");
    }
}
