package Game;

public class Lux extends Roles {
    public Lux(String name) {
        super(name);
    }

    @Override
    public void doAttack() {
        System.out.println("我是" + getName() + "武器是魔法棒");
    }


    @Override
    public void doQ(String magic) {
        System.out.println("使用技能" + magic + "攻擊");
    }

    @Override
    public void doW(String magic) {
        System.out.println("使用技能" + magic + "攻擊");
    }

    @Override
    public void doE(String magic) {
        System.out.println("使用技能" + magic + "攻擊");
    }


    @Override
    public void doSpecialAttack(String SpecialAttack) {
        System.out.println("開大招" + SpecialAttack);
    }

}
