package Game;

/**
 * Interface for common behaviors
 */
public interface IActions {
    public void doAttack();


    public void doQ(String magic);

    public void doW(String magic);

    public void doE(String magic);

    public void doSpecialAttack(String SpecialAttack);
}
